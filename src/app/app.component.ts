import { Component } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    /* ******************** data table test ******************* */
  	displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol'];
	ELEMENT_DATA: any[] = [
	  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
	  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
	  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
	  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
	  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
	  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
	  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
	  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
	  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
	  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
	];
	onSelecteRows(event){
		console.log(event);
	}


 	/* ******************** form test ******************* */
    formControls = [
        {name : 'checkBox', lable : 'check Box', input: 'checkBox', validators: [Validators.required]},
    	{name : 'code', lable : 'code', input: 'text', type: 'text', validators : [Validators.required]},
    	        {name : 'togler', lable : 'toggle', input: 'toggler', validators: [Validators.required]},
    	{name : 'lable', lable : 'lable', input: 'text', type: 'text', validators: [Validators.required]},
    	{
    		name : 'radio', items : [{name : 'radio 1', value:1},{name : 'radio 2', value:2}], 
    		input: 'radio', validators: [Validators.required]
    	},
    	{name : 'price', lable : 'price', input: 'text', type: 'number', validators: [Validators.required]},
    	{
    		name : 'select', lable : 'select item', options : [{name : 'radio 1', value:1},{name : 'radio 2', value:2}], 
    		input: 'select', validators: [Validators.required]
    	},
    	{name : 'description', lable : 'description', input: 'textArea', type: 'text', validators: [Validators.required]},
		{
			name : 'slider', lable : 'slider', input: 'slider', 
			max: 100, min: 0, step: 20,validators: [Validators.required]
		},
		{name : 'dateinput', lable : 'date input', input: 'date', type: 'date', validators: [Validators.required]},
    ];

    onSubmit(data){
    	console.log(data);
    }
    onCancel(event){

	}
	






	/* ******************************** view ************************** */
	dataObject ={name: 'mohamed qorany', 
	name_first: 'mohamed qorany', email: 'email@gmail.com'};
}
