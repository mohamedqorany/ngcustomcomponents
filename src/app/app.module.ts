import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CTableComponent } from './c-table/c-table.component';
import {MatTableModule, MatInputModule, MatPaginatorModule, MatSortModule, MatFormFieldModule,
MatCheckboxModule, MatButtonModule, MatRadioModule, MatSelectModule, MatSlideToggleModule,
MatSliderModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CFormComponent } from './c-form/c-form.component';
import {CViewComponent} from './c-view/c-view.component';

@NgModule({
  declarations: [
    AppComponent,
    CTableComponent,
    CFormComponent,
    CViewComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, MatInputModule,
    MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatCheckboxModule,
    MatButtonModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatSliderModule,
    MatDatepickerModule, MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
