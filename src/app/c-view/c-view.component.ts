import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'app-c-view',
  templateUrl: './c-view.component.html',
  styleUrls: ['./c-view.component.css']
})
export class CViewComponent implements OnChanges {

  @Input() data: any={};
  titles=[];
  constructor() { }

  ngOnChanges() {
    this.titles = Object.keys(this.data).map(title => title.replace('_', ' '));
   }
}
