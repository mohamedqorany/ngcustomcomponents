import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-c-form',
  templateUrl: './c-form.component.html',
  styleUrls: ['./c-form.component.css']
})
export class CFormComponent implements OnChanges {

  @Input() formControls:any;
  @Output() submitEvent = new EventEmitter<any>();
  @Output() cancelEvent = new EventEmitter<any>();

  customForm: FormGroup;
  constructor(private _fb: FormBuilder) { }
  ngOnChanges() {
   this.customForm = this.formControls ? this._fb.group(this.createForm()) : this._fb.group({}) ;
  }


   createForm(): any{
	    const formGroup = {};
	    for (const control of this.formControls){formGroup[control.name] = new FormControl('' , control.validators);}
	   return formGroup;
	}
   get(controllName): any {return this.formControls.get(controllName);}
   getControls(frmGrp: FormGroup, key: string) {return (<FormArray>frmGrp.controls[key]).controls;}

   submit(model){this.submitEvent.emit(model);}
   cancel(){this.cancelEvent.emit();}
}
