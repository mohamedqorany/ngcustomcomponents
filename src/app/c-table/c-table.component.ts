import {Component, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-c-table',
  templateUrl: './c-table.component.html',
  styleUrls: ['./c-table.component.css']
})
export class CTableComponent implements OnChanges {

  @Input() dataListInput=[];
  @Input() displayedColumns: string[] = [];
  @Input() selectable;
  @Output() selectedRowsEvent = new EventEmitter<any>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);

  constructor() { }

  ngOnChanges() {
    this.dataSource = new MatTableDataSource(this.dataListInput);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
   }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
        this.selectedRowsEvent.emit(this.selection.selected);
  }

  selectionChange() {
  	this.selectedRowsEvent.emit(this.selection.selected);
  }

}
